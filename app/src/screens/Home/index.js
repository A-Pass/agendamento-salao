import React, { useState, useEffect } from 'react';
import { Platform, RefreshControl } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { requests, PERMISSIONS, request } from 'react-native-permissions';
import Geolocation from '@react-native-community/geolocation';
import Api from '../../Api';
import BarberItem from '../../components/BarberItem';
import { 
  Container,
  Scroller,
  
  HeaderArea,
  HeaderTitle,
  SearchButton,

  LocationArea,
  LocationInput,
  LocationFinder,

  LoadingIcon,
  ListArea
} from './styles';

import SearchIcon from '../../assets/search.svg';
import MyLocationIcon from '../../assets/my_location.svg';

export default () => {
  const navigation = useNavigation();

  const [locationField, setLocationField] = useState('');
  const [coords, setCoords] = useState(null);
  const [loading, setLoading] = useState(false);
  const [list, setList] = useState([]);

  const handleLocationFinder = async () => {
    setCoords(null);
    let result = await request(
      Platform.OS === 'ios' 
        ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE 
        : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
    );

    if(result === 'granted') {
      setLoading(true);
      setLocationField('');
      setList([]);

      Geolocation.getCurrentPosition((info) => {

        setCoords(info.coords);

      })
    }

    getBarbers();
  };

  const handleLocationSearch = () => {
    setCoords({});
    getBarbers();
  }

  const getBarbers = async () => {
    setLoading(true);
    setList([]);

    let lat = null;
    let lng = null;

    if (coords) {
      console.log(coords);
      //lat = coords.latitude;
      //lng = coords.longitude;
    }

    const barbers = await Api.getBarbers(lat, lng, locationField);

    if (barbers.error == '') {
      setList(barbers.data);
      setLocationField(barbers.loc);
      setLoading(false);
    } else {
      alert(`Erro: ${barbers.error}`);
    }
  }

  useEffect(()=> {
    getBarbers();
  }, []);

  const onRefresh = () => {
    getBarbers();
  }

  return (
    <Container>
      <Scroller refreshControl={
        <RefreshControl refreshing={false} onRefresh={onRefresh} />
      }>
        
        <HeaderArea>
          <HeaderTitle numberOfLines={2}>Encontre o seu barbeiro favorito</HeaderTitle>
          <SearchButton onPress={()=>navigation.navigate('Search')}>
            <SearchIcon width="26" height="26" fill="#fff" />
          </SearchButton>
        </HeaderArea>
        
        <LocationArea>
          <LocationInput
            placeholder="Onde você está?"
            placeholderTextColor="#fff"
            value={locationField}
            onChangeText={t=>setLocationField(t)}
            onEndEditing={handleLocationSearch}
          />
          <LocationFinder onPress={handleLocationFinder}>
            <MyLocationIcon width="24" height="24" fill="#fff"/>
          </LocationFinder>
        </LocationArea>

        { loading && <LoadingIcon size="large" color="#fff"/> }

        <ListArea>
          {list.map((item, k) => (
            <BarberItem key={k} data={item} />
          ))}
        </ListArea>
      </Scroller>
    </Container>
  );
};
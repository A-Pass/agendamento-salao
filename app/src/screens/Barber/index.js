import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import Api from '../../Api';
import Swiper from 'react-native-swiper';

import { 
  Container,
  Scroller,
 
  FakeSwiper,
  SwipeDot,
  SwipeItem,
  SwipeImage,
  
  PageBody,
  BarberInfoArea,
  BarberAvatar,
  BarberInfo,
  BarberName,

  ServiceArea,
  TestimonialArea
} from './styles';

import Stars from '../../components/Stars';

export default () => {
  const navigation = useNavigation();
  const route = useRoute();

  const [barberInfo, setBarberInfor] = useState(route.params.barber);

  const [loading, setLoading] = useState(false);

  useEffect(()=>{
    const loadBarberInfo = async () => {
      setLoading(true);

      let response = await Api.getBarber(barberInfo.id);
      if(response.error == '') {
        setBarberInfor(response.data);
      } else {
        alert(`Error:${response.error}`);
      }

      setLoading(false);
    }
    loadBarberInfo();
  }, [])

  return (
    <Container>
      <Scroller>
        {barberInfo.photos && barberInfo.photos.length > 0 ?
          <Swiper
            style={{height: 240}}
            dot={<SwipeDot />}
            activeDot={<SwipeDot actived={true} />}
            paginationStyle={{left: null, top: 15, right: 15, bottom: null}}
            autoplay={true}
          >
            {barberInfo.photos.map((item, key)=>(
              <SwipeItem key={key}>
                <SwipeImage source={{uri:item}} resizeMode="cover" />
              </SwipeItem>
            ))}
          </Swiper>
          :
          <FakeSwiper />
        }
        <PageBody>
          <BarberInfoArea>
            <BarberAvatar source={{uri: barberInfo.avatar}}/>
            <BarberInfo>
              <BarberName>{barberInfo.name}</BarberName>
              <Stars showNumber={true} />
            </BarberInfo>
          </BarberInfoArea>
          <ServiceArea>

          </ServiceArea>
          <TestimonialArea>

          </TestimonialArea>
        </PageBody> 
      </Scroller>
    </Container>
  );
}
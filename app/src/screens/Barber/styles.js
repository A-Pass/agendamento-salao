import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: #fff;
`;

export const Scroller = styled.ScrollView`
  flex: 1;
`;

export const FakeSwiper = styled.View`
  width: 10px;
  height: 10px;
  background-color: #fff;
  border-radius: 5px;
  margin: 3px;
`;

export const SwipeDot = styled.View`
  width: 10px;
  height: 10px;
  background-color: rgba(256, 256, 256, ${ props => props.actived ? 1 : 0.5 });
  border: solid 2px #fff;
  border-radius: 50px;
  margin: 3px;
`;

export const SwipeItem = styled.View`
  flex: 1;
  background-color: #63c2d1;
`;

export const SwipeImage = styled.Image`
  width: 100%;
  height: 240px;
`;

export const PageBody = styled.View`
  background-color: #fff;
  border-top-left-radius: 60px; 
  margin-top: -50px;
  min-height: 400px;
`;

export const BarberInfoArea = styled.View`
  flex: 1;
  flex-direction: row;
`;

export const BarberAvatar = styled.Image`
  background-color: #fff;
  width: 100px;
  height: 100px;
  border-radius: 50px;
  margin-top: -40px;
  margin-left: 50px;
`;

export const BarberInfo = styled.View``;

export const BarberName = styled.Text`
  font-size: 16px;
  font-weight: bold;
`;

export const ServiceArea = styled.View``;

export const TestimonialArea = styled.View``;
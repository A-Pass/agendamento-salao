import React from 'react';
import styled from 'styled-components/native';

import Stars from './Stars';
import { useNavigation } from '@react-navigation/native';

const Area = styled.TouchableOpacity`
  background-color: #fff;
  margin-bottom: 20px;
  border-radius: 20px;
  padding: 15px;
  flex-direction: row;
`;

const Avatar = styled.Image`
  width: 88px;
  height: 88px;
  border-radius: 20px;
`;

const InfoArea = styled.View`
  margin-left: 20px;
  justify-content: space-between;
`;

const UserName = styled.Text`
  font-size: 17px;
  font-weight: bold;
`;

const SeeProfileView = styled.TouchableOpacity`
  width: 85px;
  height: 26px;
  border: solid 1px #4eadbe;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
`;

const SeeProfileViewText = styled.Text`
  font-size: 13px;
  color: #268596;
`;

export default ({data}) => {
  const navigation = useNavigation();
  
  const handleClick = () => {
    navigation.navigate('Barber', {
      barber: data
    })
  }

  return (
    <Area onPress={handleClick}>
      <Avatar source={{uri: data.avatar}}/>

      <InfoArea>
        <UserName>{data.name}</UserName>

        <Stars stars={data.stars} showNumber={true} />

        <SeeProfileView>
          <SeeProfileViewText>Ver perfil</SeeProfileViewText>
        </SeeProfileView>
      </InfoArea>
    </Area>
  );
};